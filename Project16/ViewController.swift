//
//  ViewController.swift
//  Project16
//
//  Created by Muri Gumbodete on 04/04/2022.
//

import UIKit
import MapKit

class ViewController: UIViewController {
  
  @IBOutlet var mapView: MKMapView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(changeMap))
    
    let london = Capital(title: "London", coordinate: CLLocationCoordinate2D(latitude: 51.507222, longitude: -0.1275), info: "Home to the 2012 Summer Olympics", wiki: "https://en.wikipedia.org/wiki/London")
    let oslo = Capital(title: "Oslo", coordinate: CLLocationCoordinate2D(latitude: 59.95, longitude: 10.75), info: "Founded over a thousand years ago", wiki: "https://en.wikipedia.org/wiki/Oslo")
    let paris = Capital(title: "Paris", coordinate: CLLocationCoordinate2D(latitude: 48.8567, longitude: 2.3508), info: "Often called the City of Light", wiki: "https://en.wikipedia.org/wiki/Paris")
    let rome = Capital(title: "Rome", coordinate: CLLocationCoordinate2D(latitude: 41.9, longitude: 12.5), info: "Has a whole country inside it", wiki: "https://en.wikipedia.org/wiki/Rome")
    let washington = Capital(title: "Washington", coordinate: CLLocationCoordinate2D(latitude: 38.895111, longitude: -77.036667), info: "Named after George himself", wiki: "https://en.wikipedia.org/wiki/Washington")
    
    mapView.addAnnotations([london, oslo, paris, rome, washington])
  }
  
  @objc func changeMap() {
    let ac = UIAlertController(title: "Change Map View", message: "How would you like to view the map?", preferredStyle: .alert)
    ac.addAction(UIAlertAction(title: "Standard", style: .default) {_ in
      self.mapView.mapType = .standard
    })
    ac.addAction(UIAlertAction(title: "Muted Standard", style: .default) {_ in
      self.mapView.mapType = .mutedStandard
    })
    ac.addAction(UIAlertAction(title: "Hybrid", style: .default) {_ in
      self.mapView.mapType = .hybrid
    })
    ac.addAction(UIAlertAction(title: "Hybrid Flyover", style: .default) {_ in
      self.mapView.mapType = .hybridFlyover
    })
    ac.addAction(UIAlertAction(title: "Satellite", style: .default) {_ in
      self.mapView.mapType = .satellite
    })
    ac.addAction(UIAlertAction(title: "Satellite Flyover", style: .default) {_ in
      self.mapView.mapType = .satelliteFlyover
    })
    present(ac, animated: true)
  }
}

extension ViewController: MKMapViewDelegate {
  func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    guard annotation is Capital else { return nil }
    
    let identifier = "Capital"
    
    var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView
    
    if annotationView == nil {
      annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
      annotationView?.canShowCallout = true
      
      let btn = UIButton(type: .detailDisclosure)
      annotationView?.rightCalloutAccessoryView = btn
    } else {
      annotationView?.annotation = annotation
    }
    annotationView?.markerTintColor = .blue
    return annotationView
  }
  
  func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
    guard let capital = view.annotation as? Capital else { return }
    
    let vc = WikiViewController()
    vc.capital = capital
    navigationController?.pushViewController(vc, animated: true)
    
//    let placeName = capital.title
//    let placeInfo = capital.info
//
//    let ac = UIAlertController(title: placeName, message: placeInfo, preferredStyle: .alert)
//    ac.addAction(UIAlertAction(title: "OK", style: .default))
//    present(ac, animated: true)
  }
}
