//
//  WikiViewController.swift
//  Project16
//
//  Created by Muri Gumbodete on 04/04/2022.
//

import UIKit
import WebKit

class WikiViewController: UIViewController {
  
  var webView: WKWebView!
  var capital: Capital?
  
  override func loadView() {
    webView = WKWebView()
    view = webView
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    guard capital?.wiki != nil else { return }
    
    title = capital?.title
    
    let url = URL(string: capital!.wiki)!
    webView.load(URLRequest(url: url))
    webView.allowsBackForwardNavigationGestures = true
  }
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destination.
   // Pass the selected object to the new view controller.
   }
   */
  
}
